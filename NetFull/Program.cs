﻿using System;
using CppCli;

namespace NetFull
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Point p0 = new Point(3, 4))
            using (Point p1 = new Point(4, 5))
            {
                Point p2 = p0.Add(p1);
                Console.WriteLine(p2);
            }
        }
    }
}
