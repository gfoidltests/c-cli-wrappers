#include "stdafx.h"
#include <msclr/auto_gcroot.h>
#include "ComplexWrapper.h"
//-----------------------------------------------------------------------------
// Has to be after the includes, otherwise build will fail
#using "NetFullLib.dll"
//-----------------------------------------------------------------------------
namespace CppCli::Internal
{
    class ComplexWrapperPrivate
    {
    public:
        msclr::auto_gcroot<NetFullLib::Complex^> Complex;
    };
}
//-----------------------------------------------------------------------------
namespace CppCli
{
    ComplexWrapper::ComplexWrapper(const double re, const double im)
    {
        _private          = new Internal::ComplexWrapperPrivate();
        _private->Complex = gcnew NetFullLib::Complex(re, im);
    }
    //-------------------------------------------------------------------------
    ComplexWrapper::~ComplexWrapper()
    {
        delete _private;
    }
    //-------------------------------------------------------------------------
    double ComplexWrapper::getRe()
    {
        return _private->Complex->Re;
    }
    //-------------------------------------------------------------------------
    double ComplexWrapper::getIm()
    {
        return _private->Complex->Im;
    }
    //-------------------------------------------------------------------------
    ComplexWrapper* ComplexWrapper::Add(ComplexWrapper* c2)
    {
        NetFullLib::Complex res = NetFullLib::Complex::operator+(*(_private->Complex.get()), *(c2->_private->Complex.get()));

        return new ComplexWrapper {res.Re, res.Im};
    }
}
