#pragma once
//-----------------------------------------------------------------------------
namespace CppCli
{
    public ref class Point
    {
    public:
        Point();
        Point(const double x, const double y);
        ~Point();

        double getX();
        double getY();

        Point^ Add(Point^ p2);

        Point^ operator+(Point^ p2);
        Point^ operator+(Point p2);

        virtual System::String^ ToString() override;
        //---------------------------------------------------------------------
    private:
        double _x;
        double _y;
    };
}
