#include "stdafx.h"
#include <msclr/auto_gcroot.h>
#include <msclr/marshal_cppstd.h>       // https://stackoverflow.com/a/1405251/347870
#include "PersonWrapper.h"
//-----------------------------------------------------------------------------
// Has to be after the includes, otherwise build will fail
#using "NetFullLib.dll"
//-----------------------------------------------------------------------------
namespace CppCli::Internal
{
    class PersonWrapperPrivate
    {
    public:
        msclr::auto_gcroot<NetFullLib::Person^> Person;
    };
}
//-----------------------------------------------------------------------------
namespace CppCli
{
    PersonWrapper::PersonWrapper(const std::string& name, const int age)
    {
        System::String^ clrName = msclr::interop::marshal_as<System::String^>(name);

        _private         = new Internal::PersonWrapperPrivate();
        _private->Person = gcnew NetFullLib::Person(clrName, age);
    }
    //-------------------------------------------------------------------------
    PersonWrapper::~PersonWrapper()
    {
        delete _private;
    }
    //-------------------------------------------------------------------------
    PersonWrapper::PersonWrapper()
    {
        _private = new Internal::PersonWrapperPrivate();
    }
    //-------------------------------------------------------------------------
    std::string PersonWrapper::ToString()
    {
        System::String^ clrString = _private->Person->ToString();
        return msclr::interop::marshal_as<std::string>(clrString);
    }
    //-------------------------------------------------------------------------
    std::unique_ptr<PersonWrapper> PersonWrapper::BreedWith(PersonWrapper& other)
    {
        NetFullLib::Person^ breed = _private->Person->BreedWith(other._private->Person.get());

        PersonWrapper* wrappedBreed    = new PersonWrapper();
        wrappedBreed->_private->Person = breed;

        return std::unique_ptr<PersonWrapper>(wrappedBreed);
    }
    //-------------------------------------------------------------------------
    PersonWrapper* PersonWrapper::BreedWith(PersonWrapper* other)
    {
        NetFullLib::Person^ breed = _private->Person->BreedWith(other->_private->Person.get());

        PersonWrapper* wrappedBreed    = new PersonWrapper();
        wrappedBreed->_private->Person = breed;

        return wrappedBreed;
    }
    //-------------------------------------------------------------------------
    void PersonWrapper::Print()
    {
        _private->Person->Print();
    }
}
