#pragma once
//-----------------------------------------------------------------------------
#include "CppCli_Dll.h"
//-----------------------------------------------------------------------------
namespace CppCli::Internal
{
    class PersonWrapperPrivate;
}
//-----------------------------------------------------------------------------
namespace CppCli
{
    // https://stackoverflow.com/a/44447110/347870
    class PersonWrapper
    {
    public:
        CppCli_DLL_API PersonWrapper(const std::string& name, const int age);
        CppCli_DLL_API ~PersonWrapper();

        CppCli_DLL_API std::string ToString();
        CppCli_DLL_API std::unique_ptr<PersonWrapper> BreedWith(PersonWrapper& other);
        CppCli_DLL_API PersonWrapper* BreedWith(PersonWrapper* other);
        CppCli_DLL_API void Print();
        //---------------------------------------------------------------------
    private:
        PersonWrapper();
        // https://mbevin.wordpress.com/2012/11/18/smart-pointers/
        //std::unique_ptr<Internal::PersonWrapperPrivate> _private;
        Internal::PersonWrapperPrivate* _private;
    };
}
