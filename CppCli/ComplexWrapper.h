#pragma once
//-----------------------------------------------------------------------------
#include "CppCli_Dll.h"
//-----------------------------------------------------------------------------
namespace CppCli::Internal
{
    class ComplexWrapperPrivate;
}
//-----------------------------------------------------------------------------
namespace CppCli
{
    class ComplexWrapper
    {
    public:
        CppCli_DLL_API ComplexWrapper(const double re, const double im);
        CppCli_DLL_API ~ComplexWrapper();

        CppCli_DLL_API double getRe();
        CppCli_DLL_API double getIm();

        CppCli_DLL_API ComplexWrapper* Add(ComplexWrapper* c2);
        //---------------------------------------------------------------------
    private:
        // https://mbevin.wordpress.com/2012/11/18/smart-pointers/
        //std::unique_ptr<Internal::ComplexWrapperPrivate> _private;
        Internal::ComplexWrapperPrivate* _private;
    };
}
