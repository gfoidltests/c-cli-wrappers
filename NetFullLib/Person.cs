﻿using System;

namespace NetFullLib
{
    public class Person
    {
        public string Name { get; }
        public int Age     { get; }
        //---------------------------------------------------------------------
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age  = age;
        }
        //---------------------------------------------------------------------
        public override string ToString() => $"Name: {this.Name}, Age: {this.Age}";
        //---------------------------------------------------------------------
        public Person BreedWith(Person other)
        {
            return new Person($"{this.Name} + {other.Name}", 0);
        }
        //---------------------------------------------------------------------
        public void Print()
        {
            Console.WriteLine(this);
        }
    }
}
