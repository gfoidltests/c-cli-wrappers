﻿namespace NetFullLib
{
    public readonly struct Complex
    {
        public double Re { get; }
        public double Im { get; }
        //---------------------------------------------------------------------
        public Complex(double re, double im)
        {
            this.Re = re;
            this.Im = im;
        }
        //---------------------------------------------------------------------
        public static Complex operator+(Complex c1, Complex c2)
        {
            return new Complex(c1.Re + c2.Re, c1.Im + c2.Im);
        }
    }
}
