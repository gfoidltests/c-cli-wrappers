﻿using System;
using System.Runtime.InteropServices;

namespace NetCore
{
    class Program
    {
        static unsafe void Main(string[] args)
        {
            void* complex = complex_create(3, 4);
            try
            {
                double im = complex_get_re(complex);
                double re = complex_get_im(complex);

                Console.WriteLine(im);
                Console.WriteLine(re);
            }
            finally
            {
                complex_destroy(complex);
            }

            Console.WriteLine("end");
        }

        [DllImport("CppCli_Cdll")]
        private static extern unsafe void* complex_create(double re, double im);

        [DllImport("CppCli_Cdll")]
        private static extern unsafe void complex_destroy(void* complex);

        [DllImport("CppCli_Cdll")]
        private static extern unsafe double complex_get_re(void* complex);

        [DllImport("CppCli_Cdll")]
        private static extern unsafe double complex_get_im(void* complex);
    }
}
