#include "stdafx.h"
#include "complex.h"
#include "ComplexWrapper.h"
//-----------------------------------------------------------------------------
using namespace CppCli;
//-----------------------------------------------------------------------------
void* complex_create(const double re, const double im)
{
    return new ComplexWrapper(re, im);
}
//-----------------------------------------------------------------------------
void complex_destroy(void* complex)
{
    ComplexWrapper* wrapper = reinterpret_cast<ComplexWrapper*>(complex);
    delete wrapper;
}
//-----------------------------------------------------------------------------
double complex_get_re(void* complex)
{
    return reinterpret_cast<ComplexWrapper*>(complex)->getRe();
}
//-----------------------------------------------------------------------------
double complex_get_im(void* complex)
{
    return reinterpret_cast<ComplexWrapper*>(complex)->getIm();
}
//-----------------------------------------------------------------------------
void* complex_add(void* complex, void* other)
{
    ComplexWrapper* c1 = reinterpret_cast<ComplexWrapper*>(complex);
    ComplexWrapper* c2 = reinterpret_cast<ComplexWrapper*>(other);

    return c1->Add(c2);
}
