#include "stdafx.h"
#include "person.h"
#include "PersonWrapper.h"
//-----------------------------------------------------------------------------
using namespace CppCli;
//-----------------------------------------------------------------------------
void* person_create(const char* name, const int age)
{
    return new PersonWrapper(std::string(name), age);
}
//-----------------------------------------------------------------------------
void person_destroy(void* person)
{
    PersonWrapper* wrapper = reinterpret_cast<PersonWrapper*>(person);
    delete person;
}
//-----------------------------------------------------------------------------
int person_to_string(void* person, char* str)
{
    PersonWrapper* wrapper           = reinterpret_cast<PersonWrapper*>(person);
    std::unique_ptr<std::string> res = std::make_unique<std::string>(wrapper->ToString());

    res->copy(str, res->length());

    return (int)(res->length());
}
//-----------------------------------------------------------------------------
void* person_breed_with(void* person, void* other)
{
    PersonWrapper* p0 = reinterpret_cast<PersonWrapper*>(person);
    PersonWrapper* p1 = reinterpret_cast<PersonWrapper*>(other);

    return p0->BreedWith(p1);
}
//-----------------------------------------------------------------------------
void person_print(void* person)
{
    PersonWrapper* wrapper = reinterpret_cast<PersonWrapper*>(person);
    wrapper->Print();
}
