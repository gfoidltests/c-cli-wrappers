#pragma once
//-----------------------------------------------------------------------------
#include "CppCli_Cdll_Dll.h"
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif
    CppCli_Cdll_DLL_API void* person_create(const char* name, const int age);
    CppCli_Cdll_DLL_API void  person_destroy(void* person);
    CppCli_Cdll_DLL_API int   person_to_string(void* person, char* str);            // https://stackoverflow.com/a/10799804/347870
    CppCli_Cdll_DLL_API void* person_breed_with(void* person, void* other);
    CppCli_Cdll_DLL_API void  person_print(void* person);
#ifdef __cplusplus
}
#endif
