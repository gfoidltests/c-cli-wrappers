#pragma once
//-----------------------------------------------------------------------------
#include "CppCli_Cdll_Dll.h"
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif
    CppCli_Cdll_DLL_API void*  complex_create(const double re, const double im);
    CppCli_Cdll_DLL_API void   complex_destroy(void* complex);
    CppCli_Cdll_DLL_API double complex_get_re(void* complex);
    CppCli_Cdll_DLL_API double complex_get_im(void* complex);
    CppCli_Cdll_DLL_API void*  complex_add(void* complex, void* other);
#ifdef __cplusplus
}
#endif
